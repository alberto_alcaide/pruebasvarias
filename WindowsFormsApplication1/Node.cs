using System;

namespace WindowsFormsApplication1
{
    internal class Node
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}