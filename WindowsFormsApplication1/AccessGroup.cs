namespace WindowsFormsApplication1
{
    internal class AccessGroup
    {
        public string Name { get; set; }
        public string EventId { get; set; }
        public int EventCalendarId { get; set; }
        public int CodeTypeId { get; set; }
        public string CodeSubTypeId { get; set; }

        public bool CorrespondeAAbonados()
        {
            return CodeTypeId == 3 && int.Parse(CodeSubTypeId) > 200;
        }

        public bool CorrespondeACortesias()
        {
            return CodeTypeId == 3 && int.Parse(CodeSubTypeId) < 100;
        }
    }
}