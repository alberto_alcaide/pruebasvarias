using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using iTextSharp.text;

namespace WindowsFormsApplication1
{
    internal class PruebasRepository
    {
        public string codigosARetornar = "5";
        public List<OperationGroup> GetGruposDeControl()
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                return conn.Query<OperationGroup>("SELECT * FROM OperationGroup ORDER BY Description").ToList();
            }
        }

        public List<Node> GetPuntosDeControl(OperationGroup og)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                return conn.Query<Node>("SELECT * FROM Node WHERE OperationGroupId = @ogid ORDER BY Name", new { ogid = og.Id }).ToList();
            }
        }

        public List<AccessGroup> GetOperacionesPunto(Node punto)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                const string acQuery = @"
SELECT ac.Name, ac.EventId, ac.EventCalendarId, cst.CodeTypeId, ac.CodeSubTypeId
FROM NodeOperation nop
INNER JOIN Operation op ON nop.OperationId = op.Id
INNER JOIN AccessGroup ac ON op.AccessGroupId1 = ac.Id
INNER JOIN CodeSubType cst ON ac.CodeSubTypeId = cst.ExternalId
WHERE nop.NodeId = @nodeId";
                return conn.Query<AccessGroup>(acQuery, new { nodeId = punto.Id }).ToList();
            }
        }

        public List<Code> GetCodigos(AccessGroup operacion, List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                string queryCodes = @"
SELECT TOP @top * 
FROM Code
WHERE CodeTypeId = @ctype 
  AND CodeSubTypeId = @csubtype
  AND Id NOT IN @used".Replace("@top", codigosARetornar);
                return conn.Query<Code>(queryCodes, new { ctype = operacion.CodeTypeId, csubtype = operacion.CodeSubTypeId, used = codigosUsados }).ToList();
            }
        }
        public List<Code> GetCodigosCortesias(AccessGroup operacion, List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                string queryCodes = @"
SELECT TOP @top * 
FROM Code
WHERE CodeTypeId = 3 
  AND CodeSubTypeId = @csubtype
  AND Id NOT IN @used".Replace("@top", codigosARetornar);
                return conn.Query<Code>(queryCodes, new { csubtype = operacion.CodeSubTypeId, used = codigosUsados }).ToList();
            }
        }

        public List<Code> GetCodigosAbonadosSinInvitados(AccessGroup operacion, List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                string queryCodes = @"
SELECT TOP @top *
FROM Code c
INNER JOIN ( 
	SELECT Ref 
	FROM Code
	WHERE CodeTypeId = @ctype 
      AND CodeSubTypeId = @csubtype
      AND Id NOT IN @used
	  AND Ref != ''
	GROUP BY Ref
	HAVING COUNT(Id) = 1
) as a ON c.Ref = a.Ref
WHERE c.CodeTypeId = @ctype 
  AND c.CodeSubTypeId = @csubtype
  AND c.Id NOT IN @used".Replace("@top", codigosARetornar);
                return conn.Query<Code>(queryCodes, new { ctype = operacion.CodeTypeId, csubtype = operacion.CodeSubTypeId, used = codigosUsados }).ToList();
            }
        }
        public List<CodeAbonado> GetCodigosAbonadosConInvitados(AccessGroup operacion, List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                const string queryCodesTitular = @"
SELECT TOP 2 *
FROM Code c
INNER JOIN ( 
	SELECT Ref, MIN(Id) as Id 
	FROM Code
	WHERE CodeTypeId = @ctype 
      AND CodeSubTypeId = @csubtype
      AND Id NOT IN @used
	  AND Ref != ''
	GROUP BY Ref
	HAVING COUNT(Id) > 1
) as a ON c.Ref = a.Ref AND c.Id = a.Id
WHERE c.CodeTypeId = @ctype 
  AND c.CodeSubTypeId = @csubtype
  AND c.Id NOT IN @used";
                var titulares = conn.Query<CodeAbonado>(queryCodesTitular, new { ctype = operacion.CodeTypeId, csubtype = operacion.CodeSubTypeId, used = codigosUsados }).ToList();
                if (titulares.Any())
                {
                    const string queryCodesInvitados = @"
SELECT * 
FROM Code
WHERE CodeTypeId = @ctype 
  AND CodeSubTypeId = @csubtype
  AND Id NOT IN @used
  AND Ref = @reftit
  AND Id != @idtit";
                    foreach (var titular in titulares)
                    {
                        titular.Invitados =
                            conn.Query<Code>(queryCodesInvitados,
                                new
                                {
                                    ctype = operacion.CodeTypeId,
                                    csubtype = operacion.CodeSubTypeId,
                                    used = codigosUsados,
                                    reftit = titular.Ref,
                                    idtit = titular.Id
                                }).ToList();
                    }
                }
                return titulares;
            }
        }

        public List<Code> GetCodigosAbonadosEmpresas(AccessGroup operacion, List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                string queryCodes = @"
SELECT TOP @top * 
FROM Code
WHERE CodeTypeId = @ctype 
  AND CodeSubTypeId = @csubtype
  AND Id NOT IN @used
  AND Ref = ''".Replace("@top", codigosARetornar);
                return conn.Query<Code>(queryCodes, new { ctype = operacion.CodeTypeId, csubtype = operacion.CodeSubTypeId, used = codigosUsados }).ToList();
            }
        }

        public List<Code> GetCodigosBloqueo101(List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                string queryCodes = @"
SELECT TOP 20 * 
FROM Code
WHERE Observation = 'Llamar a Supervisor. Restricción 101'
  AND Id NOT IN @used";
                return conn.Query<Code>(queryCodes, new { used = codigosUsados }).ToList();
            }
        }

        public List<Code> GetCodigosBloqueo102(List<Guid> codigosUsados)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                string queryCodes = @"
SELECT TOP 20 * 
FROM Code
WHERE Observation = 'Llamar a Supervisor. Restricción 102'
  AND Id NOT IN @used";
                return conn.Query<Code>(queryCodes, new { used = codigosUsados }).ToList();
            }
        }
    }
}