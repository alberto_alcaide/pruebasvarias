using System;

namespace WindowsFormsApplication1
{
    internal class OperationGroup
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}