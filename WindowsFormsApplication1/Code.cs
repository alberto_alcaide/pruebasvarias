using System;

namespace WindowsFormsApplication1
{
    internal class Code
    {
        public Guid Id { get; set; }
        public string CodeString { get; set; }
        public string ParentCodeString { get; set; }
        public string Ref { get; set; }
        public int CodeTypeId { get; set; }
        public string CodeSubTypeId { get; set; }
    }
}