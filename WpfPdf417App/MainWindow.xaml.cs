﻿using System;
using System.Drawing;
using System.Text;
using System.Windows;
using iTextSharp.text.pdf;
using ZXing;
using ZXing.PDF417;
using ZXing.PDF417.Internal;
using ZXing.Rendering;
using Color = System.Drawing.Color;

namespace WpfPdf417App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var timbre =
                "<TED version=\"1.0\"><DD><RE>89793400-0</RE><TD>34</TD><F>1</F><FE>2006-04-11</FE><RR>96555640-0</RR><RSR>Comercial Itala S.A.</RSR><MNT>370000</MNT><IT1>Asesor: webmanagement de sitio web.</IT1><CAF version=\"1.0\"><DA><RE>89793400-0</RE><RS>CARLOS MARSH Y COMPANIA LIMITADA</RS><TD>34</TD><RNG><D>1</D><H>1000</H></RNG><FA>2006-04-04</FA><RSAPK><M>1/v1oBs3vd55zKNOV87E0Ak0TRWhDAhPTjmhsshnmV3ow0JLrCegyTKJxQGZMsPQk2FRz1+IY3jsbB8+/IwtmQ==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">VsIzUSkloXnpMaCMia8SA8/rmkhxyxaFzeTFfa5S59ZNeSOW+bPEB64cMXESE5HOC4OKrrmzQBBe1o8ur6tcvQ==</FRMA></CAF><TSTED>2006-04-11T15:29:26</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">XuKeTZMUmfEim8brMIDSnlJrHriaKnP6BTSnN8foWAotyoCX2ro9faocAIssI2Ukv+INmCCi80KJvaaR5HrnnA==</FRMT></TED>";
            //var bmp = GenerarPdf417(timbre);
            var bmp = GeneraPdf417Itext(timbre);
            var bmpSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(bmp.GetHbitmap(), IntPtr.Zero,
                Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            imgCodigo.Source = bmpSource;

        }

        private Bitmap GenerarPdf417(string data)
        {
            var writer = new BarcodeWriter { Renderer = new BitmapRenderer(), Format = BarcodeFormat.PDF_417 };
            var encOptions = new PDF417EncodingOptions();
            encOptions.Compaction = Compaction.BYTE;
            encOptions.Compact = false;
            encOptions.PureBarcode = true;
            encOptions.Width = 3;
            encOptions.Height = 1;
            encOptions.Dimensions = new Dimensions(3, 11, 10, 60);
            //IDictionary<EncodeHintType, object> hints = new Dictionary<EncodeHintType, object>();
            //encOptions.Hints = hints;
            writer.Options = encOptions;

            return writer.Write(data);
        }

        private Bitmap GeneraPdf417Itext(string data)
        {
            BarcodePDF417 bcode = new BarcodePDF417();
            bcode.CodeRows = 5;
            bcode.CodeColumns = 13;
            bcode.ErrorLevel = 5;
            bcode.LenCodewords = 999;
            bcode.Options = BarcodePDF417.PDF417_FORCE_BINARY | BarcodePDF417.PDF417_FIXED_RECTANGLE;
            bcode.Text = Encoding.GetEncoding("ISO-8859-1").GetBytes(data);

            return new Bitmap(bcode.CreateDrawingImage(Color.Black, Color.White));
        }
    }
}
