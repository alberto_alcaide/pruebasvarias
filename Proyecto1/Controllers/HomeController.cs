﻿using System.Security.Cryptography;
using Proyecto1.Models;
using System.Web.Mvc;
using System.Linq;
using System.Web.Helpers;
using System;
using Newtonsoft.Json;

namespace Proyecto1.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Examples()
        {
            return View();
        }

        public ActionResult AngularStuff()
        {
            return View();
        }

        public JsonResult ObtenerChecklistEvento()
        {
            string eventoID = "LOL001";
            using (var db = new EventoDbContext())
            {
                var evento = db.Eventos.Find(eventoID);
                if (!evento.Items.Any())
                {
                    var grupos = from b in db.GruposChecklist select b;
                    foreach (var grupo in grupos)
                    {
                        foreach (var item in grupo.Items)
                        {
                            evento.Items.Add(new EventoChecklistItem
                            {
                                Item = item,
                                Valor = false,
                                FechaModificacion = DateTime.Now
                            });
                        }
                    }
                    db.SaveChanges();
                }

                var checklist = from ev in db.EventoChecklistItems 
                                where ev.EventoId == eventoID 
                                select new EventoChecklistViewModel { 
                                    Id = ev.Id, 
                                    EventoId = ev.EventoId, 
                                    ItemId = ev.Item.Id, 
                                    ItemName =  ev.Item.Nombre, 
                                    ItemDescription = ev.Item.Descripcion,
                                    GrupoId =  ev.Item.Grupo.Id,
                                    GrupoName = ev.Item.Grupo.Nombre,
                                    GrupoDescription = ev.Item.Grupo.Descripcion,
                                    Valor = ev.Valor, 
                                    FechaModificacion = ev.FechaModificacion
                                };
                return Json(checklist.ToList(), JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class EventoChecklistViewModel
    {
        public int Id { get; set; }
        public string EventoId { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public int GrupoId { get; set; }
        public string GrupoName { get; set; }
        public string GrupoDescription { get; set; }
        public bool Valor { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
