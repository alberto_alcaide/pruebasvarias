﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto1.Models
{
    public class GrupoChecklist
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public virtual List<ItemChecklist> Items { get; set; }
    }
}
