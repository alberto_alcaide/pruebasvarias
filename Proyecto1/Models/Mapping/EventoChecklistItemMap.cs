﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Proyecto1.Models.Mapping
{
    public class EventoChecklistItemMap : EntityTypeConfiguration<EventoChecklistItem>
    {
        public EventoChecklistItemMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Valor)
                .IsRequired();

            this.Property(t => t.FechaModificacion)
                .IsRequired();

            this.HasRequired(t => t.Item)
                .WithMany()
                .HasForeignKey(fk => fk.ItemId);

            this.HasRequired(t => t.Evento)
                .WithMany(e => e.Items)
                .HasForeignKey(fk => fk.EventoId);

            this.ToTable("EventoChecklistItems");
        }
    }
}