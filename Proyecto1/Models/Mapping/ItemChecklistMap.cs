﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Proyecto1.Models.Mapping
{
    public class ItemChecklistMap : EntityTypeConfiguration<ItemChecklist>
    {
        public ItemChecklistMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .IsMaxLength();

            this.HasRequired(t => t.Grupo)
                .WithMany(t => t.Items)
                .HasForeignKey(fk => fk.GrupoId);

            this.ToTable("ItemsChecklist");
        }
    }
}