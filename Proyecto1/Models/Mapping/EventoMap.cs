﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Proyecto1.Models.Mapping
{
    public class EventoMap : EntityTypeConfiguration<Evento>
    {
        public EventoMap()
        {
            this.HasKey(t => t.EventoId);

            this.Property(t => t.NombreEvento)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Descripcion)
                .IsRequired()
                .IsMaxLength();

            this.ToTable("Eventos");
        }
    }
}