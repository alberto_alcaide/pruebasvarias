﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Proyecto1.Models.Mapping
{
    public class GrupoChecklistMap : EntityTypeConfiguration<GrupoChecklist>
    {
        public GrupoChecklistMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Nombre)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.Descripcion)
                .IsMaxLength();

            this.ToTable("GruposChecklist");
        }
    }
}