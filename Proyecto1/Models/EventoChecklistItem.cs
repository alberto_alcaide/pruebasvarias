﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto1.Models
{
    public class EventoChecklistItem
    {
        public int Id { get; set; }
        public virtual Evento Evento { get; set; }
        public ItemChecklist Item { get; set; }
        public bool Valor { get; set; }
        public DateTime FechaModificacion { get; set; }

        public int ItemId { get; set; }
        public string EventoId { get; set; }
    }
}
