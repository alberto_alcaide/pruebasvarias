﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto1.Models
{
    public class Evento
    {
        public string EventoId { get; set; }
        public string NombreEvento { get; set; }
        public string Descripcion { get; set; }

        public virtual List<EventoChecklistItem> Items { get; set; }
    }
}