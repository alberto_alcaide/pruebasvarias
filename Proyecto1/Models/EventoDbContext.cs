﻿using Proyecto1.Models.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Proyecto1.Models
{
    public class EventoDbContext : DbContext
    {
        public EventoDbContext() : base ("Name=EventoDbContext")
        { 
        }

        public DbSet<Evento> Eventos { get; set; }
        public DbSet<GrupoChecklist> GruposChecklist { get; set; }
        public DbSet<ItemChecklist> ItemsChecklist { get; set; }
        public DbSet<EventoChecklistItem> EventoChecklistItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EventoMap());
            modelBuilder.Configurations.Add(new GrupoChecklistMap());
            modelBuilder.Configurations.Add(new ItemChecklistMap());
            modelBuilder.Configurations.Add(new EventoChecklistItemMap());
        }

    }
}