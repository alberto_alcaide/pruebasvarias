﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proyecto1.Models
{
    public class ItemChecklist
    {
        public int Id { get; set; }
        public GrupoChecklist Grupo { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public int GrupoId { get; set; }
    }
}
