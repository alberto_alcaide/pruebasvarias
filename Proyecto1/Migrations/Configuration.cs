namespace Proyecto1.Migrations
{
    using Proyecto1.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Proyecto1.Models.EventoDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Proyecto1.Models.EventoDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            context.Eventos.AddOrUpdate(
                new Evento { NombreEvento = "LOLAPALLOOZA", Descripcion = "El mayor evento en la historia de la musica chilena con artistas internacionales y wea", EventoId = "LOL001" }
                );

            var grupoSistema = new GrupoChecklist { Nombre = "En Sistema", Descripcion = "Items para revisar directamente en el sistema en las p�ginas donde se muestran detalles del evento" };
            var grupoGrafica = new GrupoChecklist { Nombre = "Gr�fica", Descripcion = "Items gr�ficos para revisar antes de ser enviados a Inform�tica para su configuraci�n en el sistema" };
            var grupoDemo = new GrupoChecklist { Nombre = "Demo", Descripcion = "Items gr�ficos para revisar antes de ser enviados a Inform�tica para su configuraci�n en el sistema" };
            var grupoFlash = new GrupoChecklist { Nombre = "Mapa Flash", Descripcion = "Items gr�ficos para revisar antes de ser enviados a Inform�tica para su configuraci�n en el sistema" };
            var grupoCompras = new GrupoChecklist { Nombre = "Compra de Tickets", Descripcion = "Items gr�ficos para revisar antes de ser enviados a Inform�tica para su configuraci�n en el sistema" };
            context.GruposChecklist.AddOrUpdate(grupoSistema, grupoGrafica, grupoDemo, grupoFlash, grupoCompras);

            context.ItemsChecklist.AddOrUpdate(
                new ItemChecklist { Nombre = "Rut Productora", Descripcion = "El RUT de la productora corresponde con el evento", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Rut Mandante", Descripcion = "El RUT del mandante corresponde para el evento (Este RUT es el que estar� impreso en los tickets)", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Nombre Artista/Evento", Descripcion = "El nombre del evento/artista es el que corresponde", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Categor�a Evento", Descripcion = "La categor�a configurada para el evento es la que corresponde", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Lugar Evento", Descripcion = "El lugar del evento es el que corresponde", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Fecha(s) Evento", Descripcion = "La o las fechas del evento se encuentran correctamente configuradas", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Hora(s) Evento", Descripcion = "La o las fechas del evento se encuentran correctamente configuradas", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Tipos Ticket", Descripcion = "El nombre de cada uno de los tipos de ticket se encuentra correctamente configurado", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Precios", Descripcion = "Los precios de cada tipo de ticket est�n correctamente configurados", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Capacidad", Descripcion = "La capacidad configurada para el evento se encuentra correcta", Grupo = grupoSistema },
                new ItemChecklist { Nombre = "Fecha(s) Evento", Descripcion = "La o las fechas del evento se encuentran correctamente configuradas", Grupo = grupoGrafica },
                new ItemChecklist { Nombre = "Fecha(s) Evento", Descripcion = "La o las fechas del evento se encuentran correctamente configuradas", Grupo = grupoGrafica }
                );

            context.SaveChanges();
        }
    }
}
