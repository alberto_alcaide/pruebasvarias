namespace Proyecto1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChecklistClasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventoChecklistItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Valor = c.Boolean(nullable: false),
                        FechaModificacion = c.DateTime(nullable: false),
                        ItemId = c.Int(nullable: false),
                        EventoId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Eventos", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.ItemsChecklist", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.EventoId)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.Eventos",
                c => new
                    {
                        EventoId = c.String(nullable: false, maxLength: 128),
                        NombreEvento = c.String(nullable: false, maxLength: 255),
                        Descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.EventoId);
            
            CreateTable(
                "dbo.ItemsChecklist",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 255),
                        Descripcion = c.String(nullable: false),
                        GrupoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GruposChecklist", t => t.GrupoId, cascadeDelete: true)
                .Index(t => t.GrupoId);
            
            CreateTable(
                "dbo.GruposChecklist",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 255),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EventoChecklistItems", "ItemId", "dbo.ItemsChecklist");
            DropForeignKey("dbo.ItemsChecklist", "GrupoId", "dbo.GruposChecklist");
            DropForeignKey("dbo.EventoChecklistItems", "EventoId", "dbo.Eventos");
            DropIndex("dbo.EventoChecklistItems", new[] { "ItemId" });
            DropIndex("dbo.ItemsChecklist", new[] { "GrupoId" });
            DropIndex("dbo.EventoChecklistItems", new[] { "EventoId" });
            DropTable("dbo.GruposChecklist");
            DropTable("dbo.ItemsChecklist");
            DropTable("dbo.Eventos");
            DropTable("dbo.EventoChecklistItems");
        }
    }
}
