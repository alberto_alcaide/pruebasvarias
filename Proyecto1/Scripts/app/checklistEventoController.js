﻿function checklistEventoController($scope, $http) {
    $http.get('ObtenerChecklistEvento')
        .success(function (data) {
            $scope.data = data;
            $scope.groupedData = _.groupBy(data, "GrupoId");
        });

    $scope.getGroupName = function(grupo) {
        return _.first(grupo).GrupoName;
    };

    $scope.getGroupDescription = function (grupo) {
        return _.first(grupo).GrupoDescription;
    };
}