﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ITextSharpFormFieldsReplacing
{
    class Program
    {
        public void CrearEticket(string archivoPlantilla, string archivoETicket, DatosEticket datos)
        {
            //Load the template PDF File using ITextSharpLibs
            using (var template = new FileStream(archivoPlantilla, FileMode.Open))
            {
                using (var eticket = new FileStream(archivoETicket, FileMode.Create))
                {
                    //open the pdf template
                    var pdfReader = new PdfReader(template);

                    //creates a pdf stamper pointing to the new file
                    var pdfStamper = new PdfStamper(pdfReader, eticket);

                    var form = pdfStamper.AcroFields;

                    /*::::: CAMPOS OBLIGATORIOS ::::::::::::*/

                    //Nro de Orden
                    form.SetField("label_nro_orden", "N° de orden:");
                    form.SetField("value_nro_orden", datos.OrdenId.ToString());

                    //Evento
                    form.SetField("label_evento", "Evento:");
                    form.SetField("value_evento", datos.NombreEvento);

                    //Lugar
                    form.SetField("label_lugar", "Lugar:");
                    form.SetField("value_lugar", datos.Lugar);
                    form.SetField("value_fecha", datos.Fecha);

                    //Sector
                    form.SetField("label_sector", "Sector:");
                    form.SetField("value_sector", datos.Sector);

                    //Precio
                    form.SetField("label_precio", "Precio:");
                    form.SetField("value_precio", datos.Precio.ToString());

                    /*::::::::: CODIGOS DE BARRA ::::::::::::::: */
                    var pdfPageContents = pdfStamper.GetOverContent(1);
                    //Codigo de barra
                    var posCb = form.GetFieldPositions("codigo_barra");
                    form.SetField("codigo_barra", "");
                    if (posCb.Any())
                    {
                        var codigoBarra = new Barcode128
                        {
                            CodeType = Barcode.CODE128,
                            Code = datos.Codigo,
                            BarHeight = 35,
                            ChecksumText = false
                        };
                        var eanimg = codigoBarra.CreateImageWithBarcode(pdfPageContents, BaseColor.BLACK, BaseColor.BLACK);
                        eanimg.RotationDegrees = 90;
                        eanimg.Rotate();
                        eanimg.SetAbsolutePosition(posCb[0].position.Left, posCb[0].position.Top - eanimg.Width);
                        pdfPageContents.AddImage(eanimg);
                    }

                    //Codigo QR
                    var posQr = form.GetFieldPositions("codigo_qr");
                    form.SetField("codigo_qr", "");
                    if (posQr.Any())
                    {
                        var qr = new BarcodeQRCode(datos.Codigo, 100, 100, null);
                        var img = qr.GetImage();
                        img.SetAbsolutePosition(posQr[0].position.Left, posQr[0].position.Top - 100); //496
                        pdfPageContents.AddImage(img, true);
                    }

                    /*::::::::::::IMAGENES DEL EVENTO ::::::::::::::::::::: */
                    var imagenTop = datos.ImagenTop;
                    var imagenBottom = datos.ImagenBottom;

                    /*::::::::::::DISCLAIMER ::::::::::::::::::::::::: */
                    var disclaimer = datos.Disclaimer;

                    /*::::: CAMPOS OPCIONALES ::::::::::::*/


                    //Cargo x Servicio
                    form.SetField("label_cargo_por_servicio", "Cargo por Servicio:");
                    form.SetField("value_cargo_por_servicio", datos.CargoPorServicio.ToString());

                    //Ubicacion
                    form.SetField("label_ubicacion", "Ubicación:");
                    form.SetField("value_ubicacion", datos.Ubicacion);

                    //RUT
                    form.SetField("label_dni", "RUT:");
                    form.SetField("value_dni", datos.Rut);

                    //Nombre
                    form.SetField("label_nombre", "Nombre:");
                    form.SetField("value_nombre", datos.Nombre);

                    //flatten the form fields
                    pdfStamper.FormFlattening = true;
                    pdfStamper.Close();
                    pdfReader.Close();
                }
            }
        }


        static void Main(string[] args)
        {
        }
    }

    public class DatosEticket
    {
        public int OrdenId { get; set; }
        public string NombreEvento { get; set; }
        public string Lugar { get; set; }
        public string Fecha { get; set; }
        public string Sector { get; set; }
        public decimal Precio { get; set; }
        public string Codigo { get; set; }
        public decimal CargoPorServicio { get; set; }
        public string Ubicacion { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string ImagenTop { get; set; }
        public string ImagenBottom { get; set; }
        public string Disclaimer { get; set; }
    }
}
