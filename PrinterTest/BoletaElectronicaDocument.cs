﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using ZXing.Common.ReedSolomon;

namespace PrinterTest
{
    public class BoletaElectronicaDocument : PrintDocument
    {
        private BoletaElectronicaTermica boletaElectronicaTermica;

        private BoletaElectronicaDocument()
        {
        }

        public BoletaElectronicaDocument(BoletaElectronicaTermica boleta)
        {
            boletaElectronicaTermica = boleta;
            this.DocumentName = "BoletaElectronica";
            string selectedPriner = null;
            //foreach (string printer in PrinterSettings.InstalledPrinters)
            //{
            //    if (printer.IndexOf("BIXOLON", StringComparison.InvariantCultureIgnoreCase) != -1)
            //    {
            //        selectedPriner = printer;
            //    }
            //}
            //if (string.IsNullOrEmpty(selectedPriner))
            //{
            //    throw new InvalidOperationException("There is no Bixolon Printer installed on your machine");
            //}
            //else
            //{
            //    PrinterSettings.PrinterName = selectedPriner;
            //}
            //PrinterSettings.PrinterName = "PDFCreator";
            PrinterSettings.PrinterName = "BIXOLON SRP-350Plus";
            PrintPage += BoletaElectronicaDocument_PrintPage;
        }

        void BoletaElectronicaDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Verdana", 20);
            SolidBrush solidBrush = new SolidBrush(Color.Black);

            PrintSection(boletaElectronicaTermica.Header);
            PrintSection(boletaElectronicaTermica.Footer);

            graphics.DrawString("Hola Mundo", font, solidBrush, 10f, 10f);
        }

        private void PrintSection(string sectionToPrint)
        {
            string[] lineas = sectionToPrint.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            foreach (var linea in lineas)
            {
                //y += height;
                //win32Print.TextOut(texto, 0, y, linea);
            }
        }

    }
}
