﻿namespace PrinterTest
{
    public class BoletaElectronicaTermica
    {
        public string Header { get; set; }
        public string Footer { get; set; }
        public string TimbreElectronicoDocumento { get; set; }
    }
}