namespace PrinterTest
{
    public abstract class VoucherBase
    {
        private int _y = 0;
        private static string _impresoraVoucher = null;
        protected Win32Print win32Print = null;
        protected int height = 0;
        protected int titulo = 0;
        protected int texto = 0;
        protected int sys = 0;
        protected int barcode = 0;
        protected int factor = 1;
        protected int pageHeight = 0;
        protected int offsetY = 0;

        internal VoucherBase()
        {
            win32Print = new Win32Print();
            _impresoraVoucher = "BIXOLON SRP-350plus";
            //_impresoraVoucher = "PDFCreator";
        }

        public virtual void Print()
        {
            win32Print.StartDoc(_impresoraVoucher);
            height = win32Print.TextMetricA.tmHeight;
            pageHeight = win32Print.PageHeight();
            offsetY = win32Print.OffsetY();
            _y = offsetY;

            if (_impresoraVoucher.ToUpper().IndexOf("BIXOLON") >= 0)
            {
                titulo = win32Print.CreateFont("FontA1x2", 0, false);
                texto = win32Print.CreateFont("FontA1x1", 0, false);
                sys = win32Print.CreateFont("FontControl", 9.5f, false);
                barcode = win32Print.CreateFont("Code128", 9.5f, false);
            }
            else
            {
                texto = win32Print.CreateFont("Verdana", 8, false);
                factor = 3;
            }
        }

        public void EndPrint()
        {
            if (sys > 0)
            {
                win32Print.StartPage();
                win32Print.TextOut(sys, 0, 0, "P");
            }
            win32Print.EndDoc();
        }

        protected int y
        {
            get { return _y; }
            set
            {
                if (value > pageHeight - 2 * height)
                {
                    _y = offsetY;
                    win32Print.StartPage();
                }
                else
                    _y = value;
            }
        }
    }
}
