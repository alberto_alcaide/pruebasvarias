﻿using System.Drawing;
using iTextSharp.text.pdf;
using Microsoft.PointOfService;
using ZXing;
using ZXing.PDF417;
using ZXing.PDF417.Internal;
using ZXing.Rendering;

namespace PrinterTest
{
    class BoletaElectronicaPOS
    {
        private PosPrinter posPrinter;

        public BoletaElectronicaPOS()
        {
            PosExplorer msPosExplorer = new PosExplorer();
            DeviceCollection devices = msPosExplorer.GetDevices(DeviceType.PosPrinter);
            foreach (DeviceInfo device in devices)
            {
                if (device.ServiceObjectName == "SRP-350plus")
                {
                    posPrinter = (PosPrinter) msPosExplorer.CreateInstance(device);
                    posPrinter.Open();
                    posPrinter.Claim(1000);
                    posPrinter.DeviceEnabled = true;
                }
            }
        }

        public void Print(string data)
        {
            //posPrinter.PrintBarCode(PrinterStation.Receipt, data, BarCodeSymbology.Pdf417, 3, 1, PosPrinter.PrinterBarCodeCenter, BarCodeTextPosition.None);
            posPrinter.PrintMemoryBitmap(PrinterStation.Receipt, GeneraPdf417Itext(data), 500, PosPrinter.PrinterBitmapCenter);
        }

        private Bitmap GenerarPdf417(string data)
        {
            var writer = new BarcodeWriter { Renderer = new BitmapRenderer(), Format = BarcodeFormat.PDF_417 };
            var encOptions = new PDF417EncodingOptions();
            encOptions.Compaction = Compaction.BYTE;
            encOptions.Compact = false;
            encOptions.PureBarcode = true;
            encOptions.Dimensions = new Dimensions(3, 11, 10, 60);
            //IDictionary<EncodeHintType, object> hints = new Dictionary<EncodeHintType, object>();
            //encOptions.Hints = hints;
            writer.Options = encOptions;

            return writer.Write(data);
        }

        private Bitmap GeneraPdf417Itext(string data)
        {
            BarcodePDF417 bcode = new BarcodePDF417();
            bcode.ErrorLevel = 5;
            bcode.SetText(data);
            var bmp = new Bitmap(bcode.CreateDrawingImage(Color.Black, Color.White));
            bmp.SetResolution(180, 180);
            return bmp;
        }
    }
}
