﻿using System.Text;
using NUnit.Framework;

namespace PrinterTest
{
    [TestFixture]
    public class PrinterTest
    {
        [Test]
        public void Print()
        {
            var boleta = new BoletaElectronicaTermica
            {
                Header = "RAZON SOCIAL PRUEBARUT.: 1-9             \r\nGIRO DEL EMISOR DEL DOCUMENTO\r\n\r\nSTA. MAGDALENA 86, PROVIDENCIA - STGO.   \r\nMAR TIRRENO 3349 LOC.7 PENALOLEN - STGO. \r\nCOMPANIA 1214 LOC.202C SANTIAGO - STGO.  \r\n\r\nBoleta Electronica Nro.: 993488707       \r\nFecha: 20-11-2013 00:00:00               \r\n\r\n-----------------------------------------\r\nDESCRIPCION\r\nUM      CANT.           UNITARIO    TOTAL\r\n-----------------------------------------\r\nPack Violetta\nL [+$ 2.000]               \r\nUN          1 x    22000           22000 \r\n-----------------------------------------\r\n\r\nTOTAL:                             22000 \r\n\r\n",
                Footer = "\r\nTIMBRE ELECTRONICO S.I.I.\r\nRES. XX del XX/XX/XXXX\r\nVerifique Documento: www.facturacion.cl/xxx/boleta \r\n\r\n\r\n                CLIENTE\r\n\r\n\r\n\r\n",
                TimbreElectronicoDocumento = "<TED version=\"1.0\"><DD><RE>89793400-0</RE><TD>34</TD><F>1</F><FE>2006-04-11</FE><RR>96555640-0</RR><RSR>Comercial Itala S.A.</RSR><MNT>370000</MNT><IT1>Asesor: webmanagement de sitio web.</IT1><CAF version=\"1.0\"><DA><RE>89793400-0</RE><RS>CARLOS MARSH Y COMPANIA LIMITADA</RS><TD>34</TD><RNG><D>1</D><H>1000</H></RNG><FA>2006-04-04</FA><RSAPK><M>1/v1oBs3vd55zKNOV87E0Ak0TRWhDAhPTjmhsshnmV3ow0JLrCegyTKJxQGZMsPQk2FRz1+IY3jsbB8+/IwtmQ==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">VsIzUSkloXnpMaCMia8SA8/rmkhxyxaFzeTFfa5S59ZNeSOW+bPEB64cMXESE5HOC4OKrrmzQBBe1o8ur6tcvQ==</FRMA></CAF><TSTED>2006-04-11T15:29:26</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">XuKeTZMUmfEim8brMIDSnlJrHriaKnP6BTSnN8foWAotyoCX2ro9faocAIssI2Ukv+INmCCi80KJvaaR5HrnnA==</FRMT></TED>"
            };

            var boletaPrinter = new BoletaElectronica(boleta);
            boletaPrinter.Print();
        }

        [Test]
        public void POSPrinterTest()
        {
            var timbre =
                "<TED version=\"1.0\"><DD><RE>89793400-0</RE><TD>34</TD><F>1</F><FE>2006-04-11</FE><RR>96555640-0</RR><RSR>Comercial Itala S.A.</RSR><MNT>370000</MNT><IT1>Asesor: webmanagement de sitio web.</IT1><CAF version=\"1.0\"><DA><RE>89793400-0</RE><RS>CARLOS MARSH Y COMPANIA LIMITADA</RS><TD>34</TD><RNG><D>1</D><H>1000</H></RNG><FA>2006-04-04</FA><RSAPK><M>1/v1oBs3vd55zKNOV87E0Ak0TRWhDAhPTjmhsshnmV3ow0JLrCegyTKJxQGZMsPQk2FRz1+IY3jsbB8+/IwtmQ==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">VsIzUSkloXnpMaCMia8SA8/rmkhxyxaFzeTFfa5S59ZNeSOW+bPEB64cMXESE5HOC4OKrrmzQBBe1o8ur6tcvQ==</FRMA></CAF><TSTED>2006-04-11T15:29:26</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">XuKeTZMUmfEim8brMIDSnlJrHriaKnP6BTSnN8foWAotyoCX2ro9faocAIssI2Ukv+INmCCi80KJvaaR5HrnnA==</FRMT></TED>";
            BoletaElectronicaPOS boleta = new BoletaElectronicaPOS();
            boleta.Print(timbre);
        }

        [Test]
        public void RawPrintTest()
        {
            var executeTestPrint = Encoding.ASCII.GetBytes("\x1d \x28 \x41 \x02 \x00 \x01 \x02");

            var timbre =
                "<TED version=\"1.0\"><DD><RE>89793400-0</RE><TD>34</TD><F>1</F><FE>2006-04-11</FE><RR>96555640-0</RR><RSR>Comercial Itala S.A.</RSR><MNT>370000</MNT><IT1>Asesor: webmanagement de sitio web.</IT1><CAF version=\"1.0\"><DA><RE>89793400-0</RE><RS>CARLOS MARSH Y COMPANIA LIMITADA</RS><TD>34</TD><RNG><D>1</D><H>1000</H></RNG><FA>2006-04-04</FA><RSAPK><M>1/v1oBs3vd55zKNOV87E0Ak0TRWhDAhPTjmhsshnmV3ow0JLrCegyTKJxQGZMsPQk2FRz1+IY3jsbB8+/IwtmQ==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">VsIzUSkloXnpMaCMia8SA8/rmkhxyxaFzeTFfa5S59ZNeSOW+bPEB64cMXESE5HOC4OKrrmzQBBe1o8ur6tcvQ==</FRMA></CAF><TSTED>2006-04-11T15:29:26</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">XuKeTZMUmfEim8brMIDSnlJrHriaKnP6BTSnN8foWAotyoCX2ro9faocAIssI2Ukv+INmCCi80KJvaaR5HrnnA==</FRMT></TED>";
            var impresoraVoucher = "BIXOLON SRP-350plus";
            //var impresoraVoucher = "PDFCreator";
            //RawPrintHelper.SendStringToPrinter(impresoraVoucher, initPrinterCmd);
            //RawPrintHelper.SendStringToPrinter(impresoraVoucher, setPrinterModeCmd);
            //Marshal.
            //RawPrintHelper.SendBytesToPrinter(impresoraVoucher, executeTestPrint, executeTestPrint.Length);
            //RawPrintHelper.SendStringToPrinter(impresoraVoucher, printAndFeedCmd);
        }

        [Test]
        public void GdiPlusPrintTest()
        {
            var boleta = new BoletaElectronicaTermica
            {
                Header = "RAZON SOCIAL PRUEBARUT.: 1-9             \r\nGIRO DEL EMISOR DEL DOCUMENTO\r\n\r\nSTA. MAGDALENA 86, PROVIDENCIA - STGO.   \r\nMAR TIRRENO 3349 LOC.7 PENALOLEN - STGO. \r\nCOMPANIA 1214 LOC.202C SANTIAGO - STGO.  \r\n\r\nBoleta Electronica Nro.: 993488707       \r\nFecha: 20-11-2013 00:00:00               \r\n\r\n-----------------------------------------\r\nDESCRIPCION\r\nUM      CANT.           UNITARIO    TOTAL\r\n-----------------------------------------\r\nPack Violetta\nL [+$ 2.000]               \r\nUN          1 x    22000           22000 \r\n-----------------------------------------\r\n\r\nTOTAL:                             22000 \r\n\r\n",
                Footer = "\r\nTIMBRE ELECTRONICO S.I.I.\r\nRES. XX del XX/XX/XXXX\r\nVerifique Documento: www.facturacion.cl/xxx/boleta \r\n\r\n\r\n                CLIENTE\r\n\r\n\r\n\r\n",
                TimbreElectronicoDocumento = "<TED version=\"1.0\"><DD><RE>89793400-0</RE><TD>34</TD><F>1</F><FE>2006-04-11</FE><RR>96555640-0</RR><RSR>Comercial Itala S.A.</RSR><MNT>370000</MNT><IT1>Asesor: webmanagement de sitio web.</IT1><CAF version=\"1.0\"><DA><RE>89793400-0</RE><RS>CARLOS MARSH Y COMPANIA LIMITADA</RS><TD>34</TD><RNG><D>1</D><H>1000</H></RNG><FA>2006-04-04</FA><RSAPK><M>1/v1oBs3vd55zKNOV87E0Ak0TRWhDAhPTjmhsshnmV3ow0JLrCegyTKJxQGZMsPQk2FRz1+IY3jsbB8+/IwtmQ==</M><E>Aw==</E></RSAPK><IDK>300</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">VsIzUSkloXnpMaCMia8SA8/rmkhxyxaFzeTFfa5S59ZNeSOW+bPEB64cMXESE5HOC4OKrrmzQBBe1o8ur6tcvQ==</FRMA></CAF><TSTED>2006-04-11T15:29:26</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">XuKeTZMUmfEim8brMIDSnlJrHriaKnP6BTSnN8foWAotyoCX2ro9faocAIssI2Ukv+INmCCi80KJvaaR5HrnnA==</FRMT></TED>"
            };
            BoletaElectronicaDocument document = new BoletaElectronicaDocument(boleta);
            document.Print();
        }

    }
}
