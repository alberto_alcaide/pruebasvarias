using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Text;
using iTextSharp.text.pdf.codec;

namespace PrinterTest
{
    public class Win32Print
    {
        #region Constantes

        private const int FW_DONTCARE = 0;
        private const int FW_THIN = 100;
        private const int FW_EXTRALIGHT = 200;
        private const int FW_ULTRALIGHT = 200;
        private const int FW_LIGHT = 300;
        private const int FW_NORMAL = 400;
        private const int FW_REGULAR = 400;
        private const int FW_MEDIUM = 500;
        private const int FW_SEMIBOLD = 600;
        private const int FW_DEMIBOLD = 600;
        private const int FW_BOLD = 700;
        private const int FW_EXTRABOLD = 800;
        private const int FW_ULTRABOLD = 800;
        private const int FW_HEAVY = 900;
        private const int FW_BLACK = 900;
        private const int ANSI_CHARSET = 0;
        private const int ARABIC_CHARSET = 178;
        private const int BALTIC_CHARSET = 186;
        private const int CHINESEBIG5_CHARSET = 136;
        private const int DEFAULT_CHARSET = 1;
        private const int EASTEUROPE_CHARSET = 238;
        private const int GB2312_CHARSET = 134;
        private const int GREEK_CHARSET = 161;
        private const int HANGEUL_CHARSET = 129;
        private const int HEBREW_CHARSET = 177;
        private const int JOHAB_CHARSET = 130;
        private const int MAC_CHARSET = 77;
        private const int OEM_CHARSET = 255;
        private const int RUSSIAN_CHARSET = 204;
        private const int SHIFTJIS_CHARSET = 128;
        private const int SYMBOL_CHARSET = 2;
        private const int THAI_CHARSET = 222;
        private const int TURKISH_CHARSET = 162;
        private const int OUT_DEFAULT_PRECIS = 0;
        private const int OUT_DEVICE_PRECIS = 5;
        private const int OUT_OUTLINE_PRECIS = 8;
        private const int OUT_RASTER_PRECIS = 6;
        private const int OUT_STRING_PRECIS = 1;
        private const int OUT_STROKE_PRECIS = 3;
        private const int OUT_TT_ONLY_PRECIS = 7;
        private const int OUT_TT_PRECIS = 4;
        private const int CLIP_DEFAULT_PRECIS = 0;
        private const int CLIP_EMBEDDED = 128;
        private const int CLIP_LH_ANGLES = 16;
        private const int CLIP_STROKE_PRECIS = 2;
        private const int ANTIALIASED_QUALITY = 4;
        private const int DEFAULT_QUALITY = 0;
        private const int DRAFT_QUALITY = 1;
        private const int NONANTIALIASED_QUALITY = 3;
        private const int PROOF_QUALITY = 2;
        private const int DEFAULT_PITCH = 0;
        private const int FIXED_PITCH = 1;
        private const int VARIABLE_PITCH = 2;
        private const int FF_DECORATIVE = 80;
        private const int FF_DONTCARE = 0;
        private const int FF_ROMAN = 16;
        private const int FF_SCRIPT = 64;
        private const int FF_SWISS = 32;

        #endregion

        #region Enumeraciones

        public enum DeviceCap
        {
            DRIVERVERSION = 0,
            TECHNOLOGY = 2,
            HORZSIZE = 4,
            VERTSIZE = 6,
            HORZRES = 8,
            VERTRES = 10,
            BITSPIXEL = 12,
            PLANES = 14,
            NUMBRUSHES = 16,
            NUMPENS = 18,
            NUMMARKERS = 20,
            NUMFONTS = 22,
            NUMCOLORS = 24,
            PDEVICESIZE = 26,
            CURVECAPS = 28,
            LINECAPS = 30,
            POLYGONALCAPS = 32,
            TEXTCAPS = 34,
            CLIPCAPS = 36,
            RASTERCAPS = 38,
            ASPECTX = 40,
            ASPECTY = 42,
            ASPECTXY = 44,

            LOGPIXELSX = 88,
            LOGPIXELSY = 90,

            SIZEPALETTE = 104,
            NUMRESERVED = 106,
            COLORRES = 108,

            PHYSICALWIDTH = 110,
            PHYSICALHEIGHT = 111,
            PHYSICALOFFSETX = 112,
            PHYSICALOFFSETY = 113,
            SCALINGFACTORX = 114,
            SCALINGFACTORY = 115
        }

        public enum TernaryRasterOperations : uint
        {
            SRCCOPY = 0x00CC0020,
            SRCPAINT = 0x00EE0086,
            SRCAND = 0x008800C6,
            SRCINVERT = 0x00660046,
            SRCERASE = 0x00440328,
            NOTSRCCOPY = 0x00330008,
            NOTSRCERASE = 0x001100A6,
            MERGECOPY = 0x00C000CA,
            MERGEPAINT = 0x00BB0226,
            PATCOPY = 0x00F00021,
            PATPAINT = 0x00FB0A09,
            PATINVERT = 0x005A0049,
            DSTINVERT = 0x00550009,
            BLACKNESS = 0x00000042,
            WHITENESS = 0x00FF0062,
            CAPTUREBLT = 0x40000000 //only if WinVer >= 5.0.0 (see wingdi.h)
        }

        #endregion

        #region Declaraciones

        [StructLayout(LayoutKind.Sequential)]
        public struct DOCINFO
        {
            public int cbSize;
            [MarshalAs(UnmanagedType.LPWStr)] public string lpszDocName;
            [MarshalAs(UnmanagedType.LPWStr)] public string lpszOutput;
            [MarshalAs(UnmanagedType.LPWStr)] public string lpszDatatype;
            public int fwType;
        }

        [CLSCompliant(true)]
        [Serializable, StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct TEXTMETRIC
        {
            public int tmHeight;
            public int tmAscent;
            public int tmDescent;
            public int tmInternalLeading;
            public int tmExternalLeading;
            public int tmAveCharWidth;
            public int tmMaxCharWidth;
            public int tmWeight;
            public int tmOverhang;
            public int tmDigitizedAspectX;
            public int tmDigitizedAspectY;
            public char tmFirstChar;
            public char tmLastChar;
            public char tmDefaultChar;
            public char tmBreakChar;
            public byte tmItalic;
            public byte tmUnderlined;
            public byte tmStruckOut;
            public byte tmPitchAndFamily;
            public byte tmCharSet;
        }

        [DllImport("winspool.Drv", EntryPoint = "GetDefaultPrinter")]
        public static extern bool GetDefaultPrinter(StringBuilder pszBuffer, ref int pcchBuffer);

        [DllImport("winspool.Drv", EntryPoint = "GetPrinter", SetLastError = true)]
        public static extern bool GetPrinter(IntPtr hPrinter, Int32 dwLevel, IntPtr pPrinter, Int32 dwBuf,
            out Int32 dwNeeded);

        [DllImport("winspool.drv", EntryPoint = "OpenPrinter", SetLastError = true)]
        internal static extern bool OpenPrinter(string PrinterName, out IntPtr PrinterHandle, int PrinterDefaults);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true)]
        internal static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("gdi32.dll", EntryPoint = "CreateDCA")]
        private static extern int CreateDC(string lpDriverName, string lpDeviceName, int lpOutput, int lpInitData);

        [DllImport("gdi32.dll", EntryPoint = "DeleteDC")]
        private static extern int DeleteDC(int hdc);

        [DllImport("gdi32.dll", EntryPoint = "TextOutA")]
        private static extern bool TextOut(int hdc, int x, int y, string lpString, int nCount);

        [DllImport("gdi32.dll", EntryPoint = "StartDocA")]
        private static extern int StartDoc(int hdc, ref DOCINFO lpdi);

        [DllImport("gdi32.dll", EntryPoint = "StartPage")]
        private static extern int StartPage(int hdc);

        [DllImport("gdi32.dll", EntryPoint = "EndDoc")]
        private static extern int EndDoc(int hdc);

        [DllImport("gdi32.dll", EntryPoint = "EndPage")]
        private static extern int EndPage(int hdc);

        [DllImport("gdi32.dll", EntryPoint = "CreateFontA")]
        private static extern int CreateFont(int nHeight, int nWidth, int nEscapement, int nOrientation, int fnWeight,
            int fdwItalic, int fdwUnderline, int fdwStrikeOut, int fdwCharSet, int fdwOutputPrecision,
            int fdwClipPrecision, int fdwQuality, int fdwPitchAndFamily, string lpszFace);

        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        private static extern int SelectObject(int hdc, int obj);

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        private static extern int DeleteObject(int hdc);

        [DllImport("gdi32.dll")]
        private static extern int GetDeviceCaps(int hdc, int nIndex);

        [DllImport("gdi32.dll", EntryPoint = "GetTextMetricsA")]
        private static extern int GetTextMetrics(int hdc, ref TEXTMETRIC lpMetrics);

        [DllImport("gdi32.dll")]
        public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth,
           int nHeight, IntPtr hObjSource, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);    



        #endregion

        private int _hPrintDC;
        private DOCINFO _di;
        private List<int> _fonts = new List<int>();
        private int _hFont;
        private int _hOldFont;
        private TEXTMETRIC _textMetric;

        public int OldFont
        {
            get { return _hOldFont; }
            set { _hOldFont = value; }
        }


        public TEXTMETRIC TextMetricA
        {
            get
            {
                if (_textMetric.tmHeight == 0)
                    GetTextMetrics(_hPrintDC, ref _textMetric);
                return _textMetric;
            }
            set { _textMetric = value; }
        }

        public static string GetDefaultPrinter()
        {
            StringBuilder dp = new StringBuilder(256);
            int size = dp.Capacity;
            if (GetDefaultPrinter(dp, ref size))
                return dp.ToString().Trim();
            else
                return null;
        }

        public void StartDoc(string printer)
        {
            int nRet;

            _hPrintDC = CreateDC(null, printer, 0, 0);

            _di = new DOCINFO();
            _di.cbSize = Marshal.SizeOf(_di);
            _di.lpszDocName = "PuntoTicket";

            nRet = StartDoc(_hPrintDC, ref _di);
            nRet = StartPage(_hPrintDC);
        }

        public void StartPage()
        {
            int nRet = EndPage(_hPrintDC);
            nRet = StartPage(_hPrintDC);
        }

        public void EndDoc()
        {
            int nRet;
            nRet = EndPage(_hPrintDC);
            nRet = EndDoc(_hPrintDC);

            foreach (int valor in _fonts)
                DeleteObject(valor);

            nRet = DeleteDC(_hPrintDC);
        }

        public int PageHeight()
        {
            int vertRes = GetDeviceCaps(_hPrintDC, (int) DeviceCap.VERTRES);
            return vertRes - OffsetY();
        }

        public int OffsetY()
        {
            return GetDeviceCaps(_hPrintDC, (int) DeviceCap.PHYSICALOFFSETY);
        }

        public int PagePhysicalHeight()
        {
            return GetDeviceCaps(_hPrintDC, (int)DeviceCap.VERTSIZE);
        }

        public int DpiX()
        {
            return GetDeviceCaps(_hPrintDC, (int)DeviceCap.LOGPIXELSX);
        }

        public int DpiY()
        {
            return GetDeviceCaps(_hPrintDC, (int)DeviceCap.LOGPIXELSY);
        }

        public int CreateFont(string face, float size, bool bold)
        {
            int nHeight = -Convert.ToInt32((size*GetDeviceCaps(_hPrintDC, (int) DeviceCap.LOGPIXELSY))/72);
            int weight = bold ? FW_BOLD : 0;
            int hFont = CreateFont(nHeight, 0, 0, 0, weight, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
                CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DECORATIVE, face);
            _fonts.Add(hFont);

            return hFont;
        }

        public void SelectFont(int hFont)
        {
            _hOldFont = SelectObject(_hPrintDC, hFont);
            _hFont = hFont;
        }

        public bool TextOut(int hFont, int x, int y, string text)
        {
            this.SelectFont(hFont);
            return TextOut(_hPrintDC, x, y, text, text.Length);
        }

        public bool TextOut(int x, int y, string text)
        {
            return TextOut(_hPrintDC, x, y, text, text.Length);
        }

        public int PrintBitmap(Bitmap image, int x, int y)
        {
            using (Graphics graphics = Graphics.FromHdc(new IntPtr(_hPrintDC)))
            {
                float unitsRatio = PageHeight() / graphics.VisibleClipBounds.Height;
                if (graphics.VisibleClipBounds.Width < image.Width)
                {
                    float maxWidth = graphics.VisibleClipBounds.Width;
                    float ratioHeight = (maxWidth*image.Height)/image.Width;

                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;

                    graphics.DrawImage(image, 0, y/unitsRatio, maxWidth, ratioHeight);
                    return (int)(ratioHeight * unitsRatio);
                }
                else
                {
                    graphics.DrawImage(image, 0, y / unitsRatio, image.Width, image.Height);
                    return (int)(image.Height * unitsRatio);
                }
            }
        }
    }
}