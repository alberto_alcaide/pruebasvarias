﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using iTextSharp.text.pdf;
using NUnit.Framework;

namespace PrinterTest
{
    public class BoletaElectronica : VoucherBase
    {
        private BoletaElectronicaTermica _boletaElectronicaTermica;
        private int _footerFont;


        public BoletaElectronica(BoletaElectronicaTermica boletaElectronicaTermica)
            : base()
        {
            _boletaElectronicaTermica = boletaElectronicaTermica;
            _footerFont = win32Print.CreateFont("FontB1x1", 0, false);
        }

        public override void Print()
        {
            base.Print();

            y += height;
            PrintSection(_boletaElectronicaTermica.Header);

            int pdfFont = win32Print.CreateFont("PDF417", 0, false);
            win32Print.TextOut(pdfFont, 0, y, _boletaElectronicaTermica.TimbreElectronicoDocumento);
            y += height;
            int vertOffset = win32Print.PrintBitmap(GeneraPdf417Itext(), 0, y);
            y += vertOffset;
            PrintSection(_boletaElectronicaTermica.Footer, _footerFont);
            base.EndPrint();
        }

        private Bitmap GeneraPdf417Itext()
        {
            BarcodePDF417 bcode = new BarcodePDF417();
            bcode.SetDefaultParameters();
            bcode.Options = BarcodePDF417.PDF417_FORCE_BINARY;
            bcode.Text = Encoding.ASCII.GetBytes(_boletaElectronicaTermica.TimbreElectronicoDocumento);
            var bmp = new Bitmap(bcode.CreateDrawingImage(Color.Black, Color.White));
            return bmp;
        }

        private void PrintSection(string sectionToPrint, int font)
        {
            string[] lineas = sectionToPrint.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            foreach (var linea in lineas)
            {
                y += height;
                win32Print.TextOut(texto, 0, y, linea);
            }
        }

        private void PrintSection(string sectionToPrint)
        {
            PrintSection(sectionToPrint, texto);
        }
    }
}